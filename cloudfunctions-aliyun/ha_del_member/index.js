"use strict";
exports.main = async (event, context) => {
  //event为客户端上传的参数
  const db = uniCloud.database();
  const member = db.collection("ha_group_person");

  let delData = await member.doc(event.memberId).remove();

  if (!event.memberId) {
    return {
      code: -2,
      msg: "当前成员ID不存在！"
    };
  } else {
    if (delData.affectedDocs && delData.deleted) {
      return {
        code: 0,
        msg: "删除成员成功！"
      };
    } else {
      return {
        code: -1,
        msg: "网络异常，请稍后重试！"
      };
    }
  }
};
