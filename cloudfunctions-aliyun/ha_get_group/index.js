"use strict";
exports.main = async (event, context) => {
  const db = uniCloud.database();
  const ha_group_list = db.collection("ha_group_list");

  let group_list = await ha_group_list
    .where({
      account_id: event.account_id
    })
    .get();
  if (group_list.data) {
    return {
      code: 0,
      data: group_list.data
    };
  } else {
    return {
      code: -1,
      msg: "网络错误！"
    };
  }
};
