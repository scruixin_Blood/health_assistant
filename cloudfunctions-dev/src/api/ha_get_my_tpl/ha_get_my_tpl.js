import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_user_template');
	let res = await collection
		.where({
			account_id: event.account_id
		})
		.get();

	let resData = []

	if (res.data.length > 0) {
		let c_template = db.collection("ha_template_list");
		for (var i = 0; i < res.data.length; i++) {
			let tmp = await c_template
				.doc(
					res.data[i].template_id
				)
				.get();
			tmp.data[0].my_tpl_id = res.data[i]._id;
			resData[i] = tmp.data[0];
		}
	}

	return resData;
}

export {
	Perform as main
}
