import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_user_account');
	let user = await collection
		.doc(
			event.account_id
		)
		.get();
	
	if (user.data && user.affectedDocs === 1){
	} else {
		return {
			status: -1,
			msg: "请重新登录！"
		}
	}

	const collection1 = db.collection('ha_user_template');
	let res1 = await collection1
		.where({
			_id: event.my_tpl_id,
			account_id: event.account_id
		})
		.remove();

	return {
		status: 0,
		msg: "删除成功！"
	}
}

export {
	Perform as main
}
