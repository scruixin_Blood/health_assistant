import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database()

async function Perform(event) {
	const collection = db.collection('ha_user_account')
	var user = await collection.doc(event.uid).get()

	if (user.data && user.data.length === 1) {
		delete user.data[0].password
		delete user.data[0].tokenSecret
		return {
			status: 0,
			data: user.data[0],
			msg: '获取成功'
		}
	} else {
		return {
			status: -1,
			msg: '用户不存在'
		}
	}

	// return user
}

export {
	Perform as main
}
