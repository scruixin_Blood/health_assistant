import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import reportFunc from '../../utils/reportFunc.js';

const db = uniCloud.database();
const dbCmd = db.command;

async function Perform(event) {
	let data = false;

	if (event.type == 0) { // 指定日期（一天）
		data = await reportFunc.getReportRecord(event.report_id,{
			create_time: dbCmd.and(dbCmd.gt(event.create_time), dbCmd.lt(event.create_time + 86400000))
		});
	} else if (event.type == 1) {
		// nothing
	}

	if (data == false) {
		return {
			status: -1,
			msg: "抱歉，没有找到相关记录！"
		}
	}

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
