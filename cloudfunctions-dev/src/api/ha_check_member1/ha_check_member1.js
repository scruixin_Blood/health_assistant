import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_group_person');
	let user = await collection
		.doc(
			event.gp_id
		)
		.get();

	if (user.data && user.affectedDocs === 1){
	} else {
		return {
			status: -1,
			msg: "团体成员不存在！"
		}
	}

	const collection1 = db.collection('ha_report_list');
	let res1 = await collection1
		.doc(
			event.report_id
		)
		.get();

	if (res1.data && res1.affectedDocs === 1){
	} else {
		return {
			status: -2,
			msg: "抱歉，报备不存在！"
		}
	}

	if ( user.data[0].group_id != res1.data[0].group_id ) {
		return {
			status: -3,
			msg: "出错了！"
		}
	}

	let data = {
		gp_id: user.data[0]._id,
		gp_info: user.data[0].person_name,
		schema: res1.data[0].content
	};

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
