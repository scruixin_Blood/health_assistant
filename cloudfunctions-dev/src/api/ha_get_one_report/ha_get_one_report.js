import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_report_list');
	let res = await collection
		.doc(
			event.report_id
		)
		.get();

	if (res.data && res.affectedDocs === 1) {
	} else {
		return {
			status: -1,
			msg: "抱歉，报备不存在！"
		}
	}

	// 报备记录
	const collection1 = db.collection('ha_report_record');
	let res1 = await collection1
		.where({
			report_id: event.report_id
		})
		.orderBy("create_time","asc")
		.get();

	let personDone = [];
	for (var i = 0; i < res1.data.length; i++) {
		if ( res1.data[i].gp_id != "") {
			personDone.push(res1.data[i].gp_id);
		}
		let tmp = await db.collection('ha_group_person')
			.doc(
				res1.data[i].gp_id
			)
			.get();

		res1.data[i].person_name = tmp.data[0].person_name;
	}

	let data = {
		self: res.data[0],
		done: res1.data
	}

	// 没完成人员列表
	if (res.data[0].check_member == 1) {
		const collection2 = db.collection('ha_group_person');
		let res2 = await collection2
			.where({
				group_id: res.data[0].group_id
			})
			.get();

		let personUndo = [];
		for (var i = 0; i < res2.data.length; i++) {
			if (!personDone.includes(res2.data[i]._id)) {
				personUndo.push(res2.data[i].person_name);
			}
		}

		data.undo = personUndo;
	}

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
