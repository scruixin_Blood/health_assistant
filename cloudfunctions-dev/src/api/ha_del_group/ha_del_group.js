import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import groupFunc from '../../utils/groupFunc.js';

async function Perform(event) {
	await groupFunc.deleteOneGroup(event.group_id);

	return {
		status: 0,
		msg: "删除成功！"
	}
}

export {
	Perform as main
}
