import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_report_list');
	let res = await collection
		.where({
			group_id: event.group_id
		})
		.get();

	return res.data;
}

export {
	Perform as main
}
