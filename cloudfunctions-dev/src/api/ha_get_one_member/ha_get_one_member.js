import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_group_person');
	let user = await collection
		.doc(
			event.member_id
		)
		.get();

	if (user.data && user.affectedDocs === 1) {
	} else {
		return {
			status: -1,
			msg: "抱歉，成员不存在！"
		}
	}

	return {
		status: 0,
		data: user.data[0],
		msg: "获取成功"
	}
}

export {
	Perform as main
}
