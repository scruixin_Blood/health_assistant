import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_user_account');
	let user = await collection
		.doc(
			event.uid
		)
		.get();
	
	if (user.data && user.affectedDocs === 1){
	} else {
		return {
			status: -1,
			msg: "请重新登录！"
		}
	}

	const collection1 = db.collection('ha_user_template');
	let res1 = await collection1
		.where({
			account_id: event.uid,
			template_id: event.tpl_id
		})
		.get();

	if (res1.data && res1.affectedDocs >= 1) {
		return {
			status: -2,
			msg: "已添加！"
		}
	}

	let data = {
		account_id: event.uid,
		template_id: event.tpl_id,
		get_way: event.get_way
	}

	let res2 = await collection1.add(data);

	return {
		status: 0,
		msg: "添加成功！"
	}
}

export {
	Perform as main
}
