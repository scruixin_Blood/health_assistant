import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import reportFunc from '../../utils/reportFunc.js';

async function Perform(event) {
	let data = await reportFunc.getOneRecord(event.record_id);

	if (data == false) {
		return {
			status: -1,
			msg: "出错了！"
		}
	}

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
